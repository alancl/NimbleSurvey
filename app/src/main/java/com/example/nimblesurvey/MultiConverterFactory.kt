package com.example.nimblesurvey

import com.undabot.izzy.retrofit.IzzyRetrofitConverter
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.lang.reflect.Type

enum class ConverterFormat {
    Json,
    JsonApi,
}

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class RequestFormat(val value: ConverterFormat)

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class ResponseFormat(val value: ConverterFormat)

class MultiConverterFactory: Converter.Factory() {
    private val jsonFactory = MoshiConverterFactory.create(moshi)
    private val jsonApiFactory = IzzyRetrofitConverter(izzi)

    override fun requestBodyConverter(
        type: Type,
        parameterAnnotations: Array<out Annotation>,
        methodAnnotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<*, RequestBody>? {
        methodAnnotations.forEach { annotation ->
            if (annotation is RequestFormat) {
                return when (annotation.value) {
                    ConverterFormat.Json -> jsonFactory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
                    ConverterFormat.JsonApi -> jsonApiFactory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
                }
            }
        }
        return jsonFactory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
    }

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        annotations.forEach { annotation ->
            if (annotation is ResponseFormat) {
                return when (annotation.value) {
                    ConverterFormat.Json -> jsonFactory.responseBodyConverter(type, annotations, retrofit)
                    ConverterFormat.JsonApi -> jsonApiFactory.responseBodyConverter(type, annotations, retrofit)
                }
            }
        }
        return jsonFactory.responseBodyConverter(type, annotations, retrofit)
    }
}