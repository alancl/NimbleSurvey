package com.example.nimblesurvey

import android.app.Application
import com.example.nimblesurvey.repository.PrefsRepository
import com.example.nimblesurvey.repository.PrefsRepositoryImpl
import com.example.nimblesurvey.repository.SurveyRepository
import com.example.nimblesurvey.repository.SurveyRepositoryImpl
import com.example.nimblesurvey.repository.UserRepository
import com.example.nimblesurvey.repository.UserRepositoryImpl
import com.example.nimblesurvey.viewmodel.HomeViewModel
import com.example.nimblesurvey.viewmodel.LoginViewModel
import com.example.nimblesurvey.viewmodel.SplashViewModel
import com.example.nimblesurvey.viewmodel.SurveyViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

private val appModule = module {
    single<UserRepository> { UserRepositoryImpl(get()) }
    single<PrefsRepository> { PrefsRepositoryImpl(get()) }
    single<SurveyRepository> { SurveyRepositoryImpl(get()) }
    viewModel { SplashViewModel(get(), get()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { SurveyViewModel(get(), get(), get()) }
}

class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(appModule)
        }
    }
}