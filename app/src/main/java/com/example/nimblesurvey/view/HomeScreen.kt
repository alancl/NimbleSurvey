@file:OptIn(ExperimentalFoundationApi::class)

package com.example.nimblesurvey.view

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nimblesurvey.R
import com.example.nimblesurvey.model.SurveySimple
import com.example.nimblesurvey.model.UserProfile
import com.example.nimblesurvey.repository.FakePrefsRepository
import com.example.nimblesurvey.repository.FakeSurveyRepository
import com.example.nimblesurvey.repository.FakeUserRepository
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme
import com.example.nimblesurvey.viewmodel.HomeViewModel
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

@Composable
fun HomeScreen(
    viewModel: HomeViewModel,
    onSurveyClick: (String) -> Unit,
    onLogoutClick: () -> Unit,
) {
    var loading by remember { mutableStateOf(false) }
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val pagerState = rememberPagerState(pageCount = { viewModel.surveys.size })
    val scope = rememberCoroutineScope()
    var retry by remember { mutableStateOf(false) }
    LaunchedEffect(retry) {
        loading = true
        viewModel.loadUserProfile()
        Log.d("HomeScreen", "loaded user profile: ${viewModel.userProfile}")
        viewModel.loadSurveys()
        loading = false
        Log.d("HomeScreen", "loaded ${viewModel.surveys.size} surveys")
        if (viewModel.userProfile == null)
            retry = true
    }
    ModalNavigationDrawer( // hardcoded to open from the left, cannot change
        drawerState = drawerState,
        drawerContent = {
            UserProfileDrawer(
                user = viewModel.userProfile,
                onLogoutClick = {
                    scope.launch {
                        val res = viewModel.logout()
                        Log.d("HomeScreen", "logout response: $res")
                        onLogoutClick()
                    }
                }
            )
        }
    ) {
        Scaffold(
            floatingActionButton = {
                FloatingButton(
                    icon = Icons.Filled.KeyboardArrowRight,
                    onClick = { viewModel.surveys.getOrNull(pagerState.currentPage)?.id?.let(onSurveyClick) },
                    modifier = Modifier.testTag("floating_button"),
                )
            },
        ) { pad ->
            HorizontalPager(state = pagerState) { index ->
                SurveyPage(survey = viewModel.surveys[index])
            }
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(
                        top = pad.calculateTopPadding(),
                        bottom = pad.calculateBottomPadding(),
                        start = 20.dp,
                        end = 20.dp,
                    ),
            ) {
                DateToday(modifier = Modifier.padding(top = 20.dp))
                IconButton(
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .padding(top = 40.dp)
                        .testTag("user_icon"),
                    onClick = {
                        scope.launch {
                            drawerState.apply {
                                if (isClosed) open() else close()
                            }
                        }
                    },
                ) {
                    UserIcon(user = viewModel.userProfile)
                }
                PageIndicator(
                    modifier = Modifier
                        .align(Alignment.BottomStart)
                        .padding(bottom = 190.dp),
                    current = pagerState.currentPage,
                    count = pagerState.pageCount,
                )
                if (loading)
                    CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
            }
        }
    }
}

@Preview
@Composable
fun HomeScreenPreview() {
    NimbleSurveyTheme {
        Surface {
            HomeScreen(
                viewModel = HomeViewModel(FakeUserRepository(), FakePrefsRepository(), FakeSurveyRepository()),
                onSurveyClick = {},
                onLogoutClick = {},
            )
        }
    }
}

private val dateFmt = SimpleDateFormat("EEEE, MMMM dd", Locale.US)

@Composable
fun DateToday(modifier: Modifier = Modifier) {
    val today = Calendar.getInstance().time
    Column(modifier = modifier) {
        Text(text = dateFmt.format(today).uppercase())
        Text(text = stringResource(id = R.string.today), fontSize = 35.sp, fontWeight = FontWeight.Bold)
    }
}

@Composable
fun UserIcon(
    user: UserProfile?,
    size: Dp = 40.dp,
) {
    AsyncImageNullable(
        modifier = Modifier
            .size(size)
            .clip(CircleShape),
        url = user?.avatar_url,
        fallback = painterResource(id = R.drawable.default_user),
    )
}

@Composable
fun SurveyPage(survey: SurveySimple) {
    Box(modifier = Modifier.fillMaxSize()) {
        AsyncImageNullable(
            modifier = Modifier.fillMaxSize(),
            url = survey.cover_image_url,
            fallback = painterResource(id = R.drawable.bg_home1),
            contentScale = ContentScale.Crop,
            colorFilter = ColorFilter.tint(Color.LightGray, BlendMode.Modulate),
        )
        Column(
            modifier = Modifier
                .align(Alignment.BottomStart)
                .height(175.dp)
                .padding(start = 20.dp, end = 20.dp, bottom = 20.dp),
            verticalArrangement = Arrangement.spacedBy(15.dp),
        ) {
            Text(
                text = survey.title,
                fontSize = 30.sp,
                fontWeight = FontWeight.Bold,
                lineHeight = 30.sp,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.testTag("survey_title"),
            )
            Text(
                modifier = Modifier.padding(end = 60.dp).testTag("survey_desc"),
                text = survey.description,
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}

@Preview
@Composable
fun SurveyPagePreview() {
    NimbleSurveyTheme {
        Surface {
            SurveyPage(survey = FakeSurveyRepository.simpleSurvey)
        }
    }
}

@Composable
fun UserProfileDrawer(
    user: UserProfile?,
    onLogoutClick: () -> Unit,
) {
    ModalDrawerSheet {
        Row(modifier = Modifier.padding(horizontal = 20.dp, vertical = 30.dp)) {
            Text(
                text = user?.name ?: stringResource(id = R.string.user),
                fontSize = 30.sp,
                modifier = Modifier.weight(1f).testTag("user_name"),
            )
            UserIcon(user)
        }
        Divider(modifier = Modifier.padding(horizontal = 20.dp))
        NavigationDrawerItem(
            modifier = Modifier.padding(10.dp).testTag("logout"),
            label = { Text(text = stringResource(id = R.string.logout), fontSize = 20.sp, color = Color.Gray) },
            selected = false,
            onClick = onLogoutClick,
        )
    }
}

@Preview
@Composable
fun UserProfileDrawerPreview() {
    NimbleSurveyTheme {
        Surface {
            UserProfileDrawer(user = FakeUserRepository.userProfile, onLogoutClick = {})
        }
    }
}