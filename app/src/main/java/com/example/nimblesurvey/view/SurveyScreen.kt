@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.nimblesurvey.view

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nimblesurvey.R
import com.example.nimblesurvey.model.Survey
import com.example.nimblesurvey.model.SurveyAnswer
import com.example.nimblesurvey.model.SurveyQuestion
import com.example.nimblesurvey.repository.FakePrefsRepository
import com.example.nimblesurvey.repository.FakeSurveyRepository
import com.example.nimblesurvey.repository.FakeUserRepository
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme
import com.example.nimblesurvey.viewmodel.SurveyViewModel
import kotlinx.coroutines.launch

@Composable
fun SurveyScreen(
    viewModel: SurveyViewModel,
    surveyId: String,
    onBackClick: () -> Unit,
) {
    var loading by remember { mutableStateOf(false) }
    var quitRequest by remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val snackbar = remember { SnackbarHostState() }
    LaunchedEffect(surveyId) {
        loading = true
        viewModel.loadSurvey(surveyId)
        loading = false
        Log.d("SurveyScreen", "loaded survey '${viewModel.survey?.title}': ${viewModel.survey?.questions?.size} questions")
    }
    val totalQuestions = viewModel.survey?.questions?.size ?: 0
    val questionIndex = viewModel.questionIndex
    val question = viewModel.currentQuestion
    Scaffold(
        topBar = {
            AppTopBar(
                onBack = if (questionIndex == null) onBackClick else null,
                onClose = if (questionIndex != null) { { quitRequest = true } } else null,
            )
        },
        snackbarHost = { SnackbarHost(hostState = snackbar) },
        floatingActionButton = {
            if (questionIndex != null && questionIndex < totalQuestions - 1)
                FloatingButton(icon = Icons.Filled.KeyboardArrowRight) {
                    viewModel.questionIndex = questionIndex + 1
                }
            else if (questionIndex == totalQuestions - 1)
                FloatingButton(text = stringResource(id = R.string.submit)) {
                    loading = true
                    Log.d("SurveyScreen", "singleStorage=${viewModel.singleStorage.toMap()}")
                    Log.d("SurveyScreen", "multiStorage=${viewModel.multiStorage.toMap()}")
                    Log.d("SurveyScreen", "textStorage=${viewModel.textStorage.toMap()}")
                    scope.launch {
                        viewModel.submitSurveyResponses()
                            .onSuccess {
                                viewModel.clearSurveyResponses()
                                onBackClick()
                            }
                            .onFailure {
                                snackbar.showSnackbar(message = it.message ?: "error", withDismissAction = true)
                            }
                        loading = false
                    }
                }
            else
                FloatingButton(text = stringResource(id = R.string.start_survey)) {
                    if (totalQuestions > 0) viewModel.questionIndex = 0
                }
        },
    ) { pad ->
        Box(modifier = Modifier.fillMaxSize()) {
            AsyncImageNullable(
                modifier = Modifier.fillMaxSize(),
                url = question?.cover_image_url ?: viewModel.survey?.cover_image_url,
                fallback = painterResource(id = R.drawable.bg_home1),
                contentScale = ContentScale.Crop,
                alpha = maxOf(0.1f, question?.cover_image_opacity ?: 1f)
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(
                        top = pad.calculateTopPadding(),
                        bottom = pad.calculateBottomPadding(),
                        start = 20.dp,
                        end = 20.dp,
                    ),
            ) {
                if (question != null)
                    SurveyQuestionDetail(
                        modifier = Modifier.fillMaxSize(),
                        question = question,
                        current = (questionIndex ?: -1) + 1,
                        total = totalQuestions,
                        singleStorage = viewModel.singleStorage,
                        multiStorage = viewModel.multiStorage,
                        textStorage = viewModel.textStorage,
                    )
                else
                    SurveyIntro(survey = viewModel.survey ?: Survey())
            }
            if (loading)
                CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        }
    }
    if (quitRequest) {
       ConfirmationDialog(
           onCancel = { quitRequest = false },
           onConfirm = { quitRequest = false; onBackClick() },
           title = stringResource(id = R.string.warning),
           text = stringResource(id = R.string.quit_confirm_msg),
       )
    }
}

@Preview
@Composable
fun SurveyScreenPreview() {
    NimbleSurveyTheme {
        Surface {
            SurveyScreen(
                viewModel = SurveyViewModel(FakeUserRepository(), FakePrefsRepository(), FakeSurveyRepository()),
                surveyId = "",
                onBackClick = {}
            )
        }
    }
}

@Composable
fun SurveyIntro(
    modifier: Modifier = Modifier,
    survey: Survey,
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(20.dp),
    ) {
        Text(
            text = survey.title,
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold,
            lineHeight = 30.sp,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
        )
        Text(
            text = survey.description,
            overflow = TextOverflow.Ellipsis,
        )
    }
}

@Preview
@Composable
fun SurveyIntroPreview() {
    NimbleSurveyTheme {
        Surface {
            SurveyIntro(survey = FakeSurveyRepository.detailSurvey)
        }
    }
}

@Composable
fun SurveyQuestionDetail(
    modifier: Modifier = Modifier,
    question: SurveyQuestion,
    current: Int,
    total: Int,
    singleStorage: MutableMap<String, String>,                  // question.id -> answer.id
    multiStorage: MutableMap<Pair<String, String>, Boolean>,    // (question.id, answer.id) -> selected
    textStorage: MutableMap<Pair<String, String>, String>,      // (question.id, answer.id) -> text
) {
    Box(modifier = modifier) {
        Column(
            verticalArrangement = Arrangement.spacedBy(5.dp),
        ) {
            val notQuestion = when (question.display_type) {
                "intro", "outro" -> true; else -> false
            }
            Text(text = "$current/$total", color = Color.Gray)
            Text(
                text = question.text,
                fontSize = if (notQuestion) 24.sp else 35.sp,
                fontWeight = FontWeight.Bold,
                lineHeight = 40.sp,
            )
            Text(text = question.help_text ?: "", fontStyle = FontStyle.Italic)
        }
        SurveyAnswerDetail(
            modifier = Modifier.align(Alignment.Center),
            answers = question.answers,
            pick = question.pick,
            display = question.display_type,
            singleChoiceValue = {
                val answerId = singleStorage[question.id]
                question.answers.indexOfFirst { it.id == answerId }
            },
            multiChoiceValue = { idx ->
                question.answers.getOrNull(idx)?.id?.let { multiStorage[Pair(question.id, it)] } ?: false
            },
            textFieldValue = { idx ->
                question.answers.getOrNull(idx)?.id?.let { textStorage[Pair(question.id, it)] } ?: ""
            },
            onSingleChoiceSelect = { idx ->
                question.answers.getOrNull(idx)?.id?.let { singleStorage[question.id!!] = it }
            },
            onMultiChoiceSelect = { idx, sel ->
                question.answers.getOrNull(idx)?.id?.let { multiStorage[Pair(question.id!!, it)] = sel }
            },
            onTextfieldChange = { idx, text ->
                question.answers.getOrNull(idx)?.id?.let { textStorage[Pair(question.id!!, it)] = text }
            }
        )
    }
}

@Preview
@Composable
fun SurveyQuestionDetailPreview() {
    NimbleSurveyTheme {
        Surface {
            val questions = FakeSurveyRepository.detailSurvey.questions
            SurveyQuestionDetail(
                modifier = Modifier.fillMaxSize(),
                question = questions[0],
                current = 1,
                total = questions.size,
                singleStorage = mutableMapOf(),
                multiStorage = mutableMapOf(),
                textStorage = mutableMapOf(),
            )
        }
    }
}

@Composable
fun SurveyAnswerDetail(
    modifier: Modifier = Modifier,
    answers: List<SurveyAnswer>,
    pick: String,
    display: String,
    singleChoiceValue: () -> Int,
    multiChoiceValue: (Int) -> Boolean,
    textFieldValue: (Int) -> String,
    onSingleChoiceSelect: (Int) -> Unit,
    onMultiChoiceSelect: (Int, Boolean) -> Unit,
    onTextfieldChange: (Int, String) -> Unit,
) {
    when (display) {
        "star", "heart", "smiley" -> {
            val (icon, color) = ratingBarSettings(display)
            RatingBar(
                modifier = modifier,
                rating = singleChoiceValue() + 1,
                maxRating = answers.size,
                icon = icon,
                activeColor = color,
                onClick = { onSingleChoiceSelect(it - 1) },
            )
        }
        "choice" -> {
            if (pick == "any") {
                MultiChoicePicker(
                    modifier = modifier,
                    choices = answers.map { it.text ?: "-" },
                    selected = { multiChoiceValue(it) },
                    onSelectionChange = { idx, sel -> onMultiChoiceSelect(idx, sel) }
                )
            } else if (pick == "one") {
                SingleChoicePicker(
                    modifier = modifier,
                    choices = answers.map { it.text ?: "-" },
                    selected = singleChoiceValue(),
                    onSelect = { onSingleChoiceSelect(it) },
                )
            }
        }
        "nps" -> {
            HorizontalChoicePicker(
                modifier = modifier,
                choices = answers.map { it.text ?: "-" },
                selected = singleChoiceValue(),
                onSelect = { onSingleChoiceSelect(it) },
            )
        }
        "dropdown" -> {
            DropdownChoice(
                modifier = modifier,
                choices = answers.map { it.text ?: "-" },
                selected = singleChoiceValue(),
                onSelect = { onSingleChoiceSelect(it) },
            )
        }
        "textfield", "textarea" -> {
            val textColor = textFieldColors()
            Column(modifier = modifier, verticalArrangement = Arrangement.spacedBy(5.dp)) {
                answers.forEachIndexed { idx, answer ->
                    TextField(
                        value = textFieldValue(idx),
                        onValueChange = { onTextfieldChange(idx, it) },
                        singleLine = display == "textfield",
                        label = answer.text?.let { { Text(it) } },
                        placeholder = answer.input_mask_placeholder?.let { { Text(text = it) } },
                        colors = textColor,
                        shape = RoundedCornerShape(10.dp),
                    )
                }
            }
        }
    }
}

private fun ratingBarSettings(display: String): Pair<ImageVector, Color> =
    when (display) {
        "heart" -> Pair(Icons.Filled.Favorite, Color.Red)
        "smiley" -> Pair(Icons.Filled.Face, Color.Yellow) // FIXME: add smiley image
        else /* star */ -> Pair(Icons.Filled.Star, Color.Yellow)
    }
