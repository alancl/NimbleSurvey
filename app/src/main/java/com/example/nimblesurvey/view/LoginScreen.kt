@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.nimblesurvey.view

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.nimblesurvey.R
import com.example.nimblesurvey.model.ApiErrors
import com.example.nimblesurvey.repository.FakePrefsRepository
import com.example.nimblesurvey.repository.FakeUserRepository
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme
import com.example.nimblesurvey.viewmodel.LoginViewModel
import kotlinx.coroutines.launch

@Composable
fun LoginScreen(
    viewModel: LoginViewModel,
    onLoginSuccess: () -> Unit,
) {
    var loading by remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val snackbar = remember { SnackbarHostState() }
    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbar) },
    ) { pad ->
        Image(
            modifier = Modifier
                .fillMaxSize()
                .blur(50.dp),
            painter = painterResource(id = R.drawable.bg_login),
            contentDescription = null,
        )
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = pad.calculateTopPadding(), bottom = pad.calculateBottomPadding()),
            contentAlignment = Alignment.Center,
        ) {
            val height = LocalConfiguration.current.screenHeightDp.dp
            Image(
                modifier = Modifier
                    .size(200.dp)
                    .offset(y = -height * 2 / 7),
                painter = painterResource(id = R.drawable.logo_white),
                contentDescription = null,
            )
            LoginForm(
                email = viewModel.email,
                passwd = viewModel.passwd,
                onEmailChange = { viewModel.email = it },
                onPasswdChange = { viewModel.passwd = it },
                onLoginClick = {
                    scope.launch {
                        loading = true
                        val res = viewModel.login()
                        loading = false
                        Log.d("LoginScreen", "login response: ${res.isSuccess}")
                        res.onSuccess {
                            onLoginSuccess()
                        }.onFailure {
                            snackbar.showSnackbar(message = (it as ApiErrors).msg)
                        }
                    }
                },
            )
            if (loading)
                CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        }
    }
}

@Preview
@Composable
fun LoginScreenPreview() {
    NimbleSurveyTheme {
        Surface {
            LoginScreen(
                viewModel = LoginViewModel(FakeUserRepository(), FakePrefsRepository()),
                onLoginSuccess = {},
            )
        }
    }
}

@Composable
fun LoginForm(
    email: String,
    passwd: String,
    onEmailChange: (String) -> Unit,
    onPasswdChange: (String) -> Unit,
    onLoginClick: () -> Unit,
) {
    Column(
        modifier = Modifier.width(IntrinsicSize.Min),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(20.dp),
    ) {
        val textColors = textFieldColors(Color.White.copy(alpha = 0.15f))
        val shape = RoundedCornerShape(10.dp)
        TextField(
            value = email,
            onValueChange = onEmailChange,
            singleLine = true,
            shape = shape,
            placeholder = { Text(stringResource(id = R.string.email)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
            colors = textColors,
        )
        TextField(
            value = passwd,
            onValueChange = onPasswdChange,
            singleLine = true,
            shape = shape,
            placeholder = { Text(stringResource(id = R.string.password)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            visualTransformation = PasswordVisualTransformation(),
            colors = textColors,
        )
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp),
            onClick = onLoginClick,
            colors = buttonColors(),
            shape = shape,
        ) {
            Text(stringResource(id = R.string.login))
        }
    }
}

@Preview
@Composable
fun LoginFormPreview() {
    NimbleSurveyTheme {
        Surface {
            LoginForm(
                email = "",
                passwd = "",
                onEmailChange = {},
                onPasswdChange = {},
                onLoginClick = {}
            )
        }
    }
}
