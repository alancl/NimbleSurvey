package com.example.nimblesurvey.view

import android.util.Log
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.nimblesurvey.R
import com.example.nimblesurvey.repository.FakePrefsRepository
import com.example.nimblesurvey.repository.FakeUserRepository
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme
import com.example.nimblesurvey.viewmodel.SplashViewModel
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.milliseconds

enum class SplashState {
    Initial,
    MoveUp,
}

@Composable
fun SplashScreen(
    viewModel: SplashViewModel,
    onFinish: (Boolean) -> Unit,
) {
    val screenHeight = LocalConfiguration.current.screenHeightDp.dp
    var animState by remember { mutableStateOf(SplashState.Initial) }
    val transition = updateTransition(targetState = animState, label = "logo anim")
    val offsetAnim by transition.animateDp(label = "offset") { state ->
        when (state) {
            SplashState.Initial -> 0.dp
            SplashState.MoveUp -> screenHeight * -2 / 7
        }
    }
    val blurAnim by transition.animateDp(label = "blur") { state ->
        when (state) {
            SplashState.Initial -> 0.dp
            SplashState.MoveUp -> 50.dp
        }
    }
    LaunchedEffect(Unit) {
        val token = viewModel.getLoginToken()
        Log.d("SplashScreen", "stored token: $token")
        if (token != null) {
            if (token.expired) {
                val res = viewModel.refreshToken(token)
                res.onSuccess {
                    Log.d("SplashScreen", "refreshed login token")
                    onFinish(true)
                }.onFailure {
                    Log.d("SplashScreen", "failed to refresh login token")
                    animState = SplashState.MoveUp // continue to login
                }
            } else
                onFinish(true)
        } else {
            delay(500.milliseconds)
            animState = SplashState.MoveUp
        }
    }
    Scaffold { pad ->
        Image(
            modifier = Modifier.fillMaxSize().blur(blurAnim),
            painter = painterResource(id = R.drawable.bg_login),
            contentDescription = null,
        )
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = pad.calculateTopPadding(), bottom = pad.calculateBottomPadding()),
            contentAlignment = Alignment.Center,
        ) {
            Image(
                modifier = Modifier.size(200.dp).offset(y = offsetAnim),
                painter = painterResource(id = R.drawable.logo_white),
                contentDescription = null,
            )
        }
    }
    if (transition.currentState == SplashState.MoveUp)
        onFinish(false)
}

@Preview
@Composable
fun SplashScreenPreview() {
    NimbleSurveyTheme {
        Surface {
            SplashScreen(
                viewModel = SplashViewModel(FakeUserRepository(), FakePrefsRepository()),
                onFinish = {},
            )
        }
    }
}
