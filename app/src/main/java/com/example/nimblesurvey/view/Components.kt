@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.nimblesurvey.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme

@Composable
fun AppTopBar(
    onBack: (() -> Unit)? = null,
    onClose: (() -> Unit)? = null,
) {
    TopAppBar(
        title = {},
        navigationIcon = {
            if (onBack != null) {
                IconButton(onClick = onBack) {
                    Icon(imageVector = Icons.Filled.KeyboardArrowLeft, contentDescription = null)
                }
            }
        },
        actions = {
            if (onClose != null) {
                IconButton(onClick = onClose) {
                    Icon(imageVector = Icons.Rounded.Close, contentDescription = null)
                }
            }
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(containerColor = Color.Transparent)
    )
}

@Preview
@Composable
fun AppTopBarPreview() {
    NimbleSurveyTheme {
        Surface {
            AppTopBar(onBack = {}, onClose = {})
        }
    }
}

@Composable
fun FloatingButton(
    modifier: Modifier = Modifier,
    icon: ImageVector,
    onClick: () -> Unit,
) {
    FloatingActionButton(
        modifier = modifier,
        shape = CircleShape,
        containerColor = Color.White,
        contentColor = Color.Black,
        onClick = onClick,
    ) {
        Icon(imageVector = icon, contentDescription = null)
    }
}

@Preview
@Composable
fun FloatingButtonIconPreview() {
    NimbleSurveyTheme {
        Surface {
            FloatingButton(icon = Icons.Filled.KeyboardArrowRight, onClick = {})
        }
    }
}

@Composable
fun FloatingButton(text: String, onClick: () -> Unit) {
    ExtendedFloatingActionButton(
        containerColor = Color.White,
        contentColor = Color.Black,
        onClick = onClick,
    ) {
        Text(text = text, fontSize = 17.sp, fontWeight = FontWeight.Bold)
    }
}

@Preview
@Composable
fun FloatingButtonTextPreview() {
    NimbleSurveyTheme {
        Surface {
            FloatingButton(text = "Button", onClick = {})
        }
    }
}

@Composable
fun AsyncImageNullable(
    modifier: Modifier = Modifier,
    url: String?,
    fallback: Painter,
    contentScale: ContentScale = ContentScale.Fit,
    alpha: Float = 1f,
    colorFilter: ColorFilter? = null,
    contentDescription: String? = null,
) {
    if (url.isNullOrEmpty()) {
        Image(
            modifier = modifier,
            painter = fallback,
            contentScale = contentScale,
            alpha = alpha,
            colorFilter = colorFilter,
            contentDescription = contentDescription,
        )
    } else {
        AsyncImage(
            modifier = modifier,
            model = url,
            fallback = fallback,
            contentScale = contentScale,
            alpha = alpha,
            colorFilter = colorFilter,
            contentDescription = contentDescription,
        )
    }
}

@Composable
fun PageIndicator(
    modifier: Modifier = Modifier,
    current: Int,
    count: Int,
    activeColor: Color = Color.LightGray,
    inactiveColor: Color = Color.Black.copy(alpha = 0.5f),
    size: Dp = 10.dp,
    spacing: Dp = 4.dp,
) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(spacing),
    ) {
        repeat(count) {
            val color = if (current == it) activeColor else inactiveColor
            Box(modifier = Modifier
                .clip(CircleShape)
                .background(color)
                .size(size))
        }
    }
}

@Preview
@Composable
fun PageIndicatorPreview() {
    NimbleSurveyTheme {
        Surface {
            PageIndicator(current = 1, count = 5)
        }
    }
}

@Composable
fun RatingBar(
    modifier: Modifier = Modifier,
    rating: Int,
    maxRating: Int = 5,
    activeColor: Color = Color.Yellow,
    inactiveColor: Color = Color.White.copy(alpha = 0.5f),
    icon: ImageVector = Icons.Filled.Star,
    onClick: (Int) -> Unit,
    spacing: Dp = 4.dp,
) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(spacing),
    ) {
        repeat(maxRating) {
            Icon(
                modifier = Modifier.clickable { onClick(it + 1) },
                imageVector = icon,
                tint = if (it < rating) activeColor else inactiveColor,
                contentDescription = null,
            )
        }
    }
}

@Preview
@Composable
fun RatingBarPreview() {
    NimbleSurveyTheme {
        Surface {
            RatingBar(rating = 3, onClick = {})
        }
    }
}

@Composable
fun SingleChoicePicker(
    modifier: Modifier = Modifier,
    choices: List<String>,
    selected: Int,
    onSelect: (Int) -> Unit,
) {
    val active = buttonColors()
    val inactive = textButtonColors()
    LazyColumn(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        itemsIndexed(choices) { idx, item ->
            if (idx == selected)
                Button(colors = active, onClick = { onSelect(idx) }) {
                    Text(item)
                }
            else
                TextButton(colors = inactive, onClick = { onSelect(idx) }) {
                    Text(item)
                }
        }
    }
}

@Preview
@Composable
fun SingleChoicePickerPreview() {
    NimbleSurveyTheme {
        Surface {
            SingleChoicePicker(
                choices = listOf("test", "foo", "bar"),
                selected = 1,
                onSelect = {}
            )
        }
    }
}

@Composable
fun HorizontalChoicePicker(
    modifier: Modifier = Modifier,
    choices: List<String>,
    selected: Int,
    onSelect: (Int) -> Unit,
) {
    val active = Color.White
    val activeInv = Color.Black
    val inactive = active.copy(alpha = 0.5f)
    Row(modifier = modifier) {
        choices.forEachIndexed { idx, item ->
            Box(
                modifier = Modifier
                    .clip(RoundedCornerShape(10.dp))
                    .background(if (idx == selected) active else Color.Transparent)
                    .clickable { onSelect(idx) },
            ) {
                Text(
                    modifier = Modifier.padding(10.dp),
                    text = item,
                    color = if (idx < selected) active else if (idx == selected) activeInv else inactive,
                )
            }
        }
    }
}

@Preview
@Composable
fun HorizontalChoicePickerPreview() {
    NimbleSurveyTheme {
        Surface {
            HorizontalChoicePicker(
                choices = listOf("1", "2", "3", "4", "5"),
                selected = 1,
                onSelect = {}
            )
        }
    }
}

@Composable
fun MultiChoicePicker(
    modifier: Modifier = Modifier,
    choices: List<String>,
    selected: (Int) -> Boolean,
    onSelectionChange: (Int, Boolean) -> Unit,
) {
    val colors = CheckboxDefaults.colors(checkedColor = Color.White)
    LazyColumn(
        modifier = modifier,
    ) {
        itemsIndexed(choices) { idx, item ->
            val sel = selected(idx)
            Row(verticalAlignment = Alignment.CenterVertically) {
                Checkbox(
                    checked = sel,
                    onCheckedChange = { onSelectionChange(idx, it) },
                    colors = colors,
                )
                Text(
                    modifier = Modifier.clickable { onSelectionChange(idx, !sel) },
                    text = item,
                )
            }
        }
    }
}

@Preview
@Composable
fun MultiChoicePickerPreview() {
    NimbleSurveyTheme {
        Surface {
            MultiChoicePicker(
                choices = listOf("test", "foo", "bar"),
                selected = { it > 0 },
                onSelectionChange = { _, _ -> }
            )
        }
    }
}

@Composable
fun DropdownChoice(
    modifier: Modifier = Modifier,
    choices: List<String>,
    selected: Int,
    onSelect: (Int) -> Unit,
) {
    var expanded by remember { mutableStateOf(false) }
    ExposedDropdownMenuBox(
        modifier = modifier,
        expanded = expanded,
        onExpandedChange = { expanded = it }
    ) {
        TextField(
            value = choices.getOrNull(selected) ?: "",
            onValueChange = {},
            readOnly = true,
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
            modifier = Modifier.menuAnchor(),
            colors = textFieldColors(),
            shape = RoundedCornerShape(10.dp),
        )
        ExposedDropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
            choices.forEachIndexed { idx, item ->
                DropdownMenuItem(
                    text = { Text(item) },
                    onClick = {
                        expanded = false
                        onSelect(idx)
                    },
                )
            }
        }
    }
}

@Preview
@Composable
fun DropdownChoicePreview() {
    NimbleSurveyTheme {
        Surface {
            DropdownChoice(
                choices = listOf("test", "foo", "bar"),
                selected = 0,
                onSelect = {},
            )
        }
    }
}

@Composable
fun ConfirmationDialog(
    onCancel: () -> Unit,
    onConfirm: () -> Unit,
    title: String,
    text: String,
) {
    AlertDialog(
        onDismissRequest = onCancel,
        title = { Text(title) },
        text = { Text(text) },
        confirmButton = {
            TextButton(onClick = onConfirm) {
                Text(stringResource(id = android.R.string.ok))
            }
        },
        dismissButton = {
            TextButton(onClick = onCancel) {
                Text(stringResource(id = android.R.string.cancel))
            }
        },
    )
}

@Preview
@Composable
fun ConfirmationDialogPreview() {
    NimbleSurveyTheme {
        Surface {
            ConfirmationDialog(onCancel = {}, onConfirm = {}, title = "Title", text = "content")
        }
    }
}

@Composable
fun textFieldColors(color: Color = Color.Black.copy(alpha = 0.5f)) =
    TextFieldDefaults.colors(
        unfocusedContainerColor = color,
        focusedContainerColor = color,
        focusedIndicatorColor = Color.Transparent,
        unfocusedIndicatorColor = Color.Transparent,
    )

@Composable
fun buttonColors(color: Color = Color.White) =
    ButtonDefaults.buttonColors(containerColor = color)

@Composable
fun textButtonColors(color: Color = Color.White) =
    ButtonDefaults.textButtonColors(contentColor = color)