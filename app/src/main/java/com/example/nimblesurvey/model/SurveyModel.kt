package com.example.nimblesurvey.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.undabot.izzy.annotations.Relationship
import com.undabot.izzy.annotations.Type
import com.undabot.izzy.models.IzzyResource
import java.util.Date

@Type("survey_simple")
data class SurveySimple(
    @JsonProperty("title") val title: String = "",
    @JsonProperty("description") val description: String = "",
    @JsonProperty("thank_email_above_threshold") val thank_email_above_threshold: String? = null,
    @JsonProperty("thank_email_below_threshold") val thank_email_below_threshold: String? = null,
    @JsonProperty("is_active") val is_active: Boolean = false,
    @JsonProperty("cover_image_url") val cover_image_url: String = "",
    @JsonProperty("created_at") val created_at: Date = Date(),
    @JsonProperty("active_at") val active_at: Date? = null,
    @JsonProperty("inactive_at") val inactive_at: Date? = null,
    @JsonProperty("survey_type") val survey_type: String = "",
): IzzyResource()

@Type("survey")
data class Survey(
    @JsonProperty("title") val title: String = "",
    @JsonProperty("description") val description: String = "",
    @JsonProperty("thank_email_above_threshold") val thank_email_above_threshold: String? = null,
    @JsonProperty("thank_email_below_threshold") val thank_email_below_threshold: String? = null,
    @JsonProperty("is_active") val is_active: Boolean = false,
    @JsonProperty("cover_image_url") val cover_image_url: String = "",
    @JsonProperty("created_at") val created_at: Date = Date(),
    @JsonProperty("active_at") val active_at: Date? = null,
    @JsonProperty("inactive_at") val inactive_at: Date? = null,
    @JsonProperty("survey_type") val survey_type: String = "",
): IzzyResource() {
    @Relationship("questions") var questions: List<SurveyQuestion> = listOf()
}

@Type("question")
data class SurveyQuestion(
    @JsonProperty("text") val text: String = "",
    @JsonProperty("help_text") val help_text: String? = null,
    @JsonProperty("display_order") val display_order: Int = 0,
    @JsonProperty("short_text") val short_text: String = "",
    @JsonProperty("pick") val pick: String = "",
    @JsonProperty("display_type") val display_type: String = "",
    @JsonProperty("is_mandatory") val is_mandatory: Boolean = false,
    @JsonProperty("correct_answer_id") val correct_answer_id: String? = null,
    @JsonProperty("facebook_profile") val facebook_profile: String? = null,
    @JsonProperty("twitter_profile") val twitter_profile: String? = null,
    @JsonProperty("image_url") val image_url: String? = null,
    @JsonProperty("cover_image_url") val cover_image_url: String? = null,
    @JsonProperty("cover_image_opacity") val cover_image_opacity: Float = 0f,
    @JsonProperty("cover_background_color") val cover_background_color: String? = null,
    @JsonProperty("is_shareable_on_facebook") val is_shareable_on_facebook: Boolean = false,
    @JsonProperty("is_shareable_on_twitter") val is_shareable_on_twitter: Boolean = false,
    @JsonProperty("font_face") val font_face: String? = null,
    @JsonProperty("font_size") val font_size: String? = null,
    @JsonProperty("tag_list") val tag_list: String = "",
): IzzyResource() {
    @Relationship("answers") var answers: List<SurveyAnswer> = listOf()
}

@Type("answer")
data class SurveyAnswer(
    @JsonProperty("text") val text: String? = null,
    @JsonProperty("help_text") val help_text: String? = null,
    @JsonProperty("input_mask_placeholder") val input_mask_placeholder: String? = null,
    @JsonProperty("short_text") val short_text: String? = null,
    @JsonProperty("is_mandatory") val is_mandatory: Boolean = false,
    @JsonProperty("is_customer_first_name") val is_customer_first_name: Boolean = false,
    @JsonProperty("is_customer_last_name") val is_customer_last_name: Boolean = false,
    @JsonProperty("is_customer_title") val is_customer_title: Boolean = false,
    @JsonProperty("is_customer_email") val is_customer_email: Boolean = false,
    @JsonProperty("prompt_custom_answer") val prompt_custom_answer: Boolean = false,
    @JsonProperty("weight") val weight: Int? = null,
    @JsonProperty("display_order") val display_order: Int = 0,
    @JsonProperty("display_type") val display_type: String = "",
    @JsonProperty("input_mask") val input_mask: String? = null,
    @JsonProperty("date_constraint") val date_constraint: String? = null,
    @JsonProperty("default_value") val default_value: String? = null,
    @JsonProperty("response_class") val response_class: String = "",
    @JsonProperty("reference_identifier") val reference_identifier: String? = null,
    @JsonProperty("score") val score: Int? = null,
    @JsonProperty("alerts") val alerts: List<String> = listOf(),
): IzzyResource()

data class ResponseRequest(
    val survey_id: String,
    val questions: List<ResponseQuestion>,
)

data class ResponseQuestion(
    val id: String,
    val answers: List<ResponseAnswer>,
)

data class ResponseAnswer(
    val id: String,
    val answer: String? = null,
)