package com.example.nimblesurvey.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.undabot.izzy.annotations.Type
import com.undabot.izzy.models.IzzyResource

data class LoginRequest(
    val grant_type: String,
    val email: String,
    val password: String,
    val client_id: String,
    val client_secret: String,
)

data class RefreshRequest(
    val grant_type: String,
    val refresh_token: String,
    val client_id: String,
    val client_secret: String,
)

data class LogoutRequest(
    val token: String,
    val client_id: String,
    val client_secret: String,
)

data class ForgotRequest(
    val user: UserEmail,
    val client_id: String,
    val client_secret: String,
)

data class UserEmail(val email: String)

data class RegistrationRequest(
    val user: RegistrationUser,
    val client_id: String,
    val client_secret: String,
)

data class RegistrationUser(
    val email: String,
    val name: String,
    val password: String,
    val password_confirmation: String
)

data class ApiErrors(val errors: List<ApiError>): RuntimeException() {
    val msg: String
        get() = errors.firstOrNull()?.let { it.detail ?: it.code } ?: "unknown"
}

data class ApiError(val detail: String?, val code: String?)

data class DataResponse<T>(
    val data: DataDetail<T>,
    val meta: MetaData?,
) {
    val value: T
        get() = data.attributes
}

data class DataDetail<T>(
    val id: String,
    val type: String,
    val attributes: T,
)

data class MetaData(
    val message: String?,
)

@Type("token")
data class LoginToken(
    @JsonProperty("access_token") val access_token: String = "",
    @JsonProperty("token_type") val token_type: String = "",
    @JsonProperty("expires_in") val expires_in: Int = 3600,
    @JsonProperty("refresh_token") val refresh_token: String = "",
    @JsonProperty("created_at") val created_at: Int = (System.currentTimeMillis() / 1000).toInt(),
): IzzyResource() {
    val expired: Boolean
        get() = created_at + expires_in < System.currentTimeMillis() / 1000
    val auth: String
        get() = "Bearer $access_token"
}

@Type("user")
data class UserProfile(
    @JsonProperty("email") val email: String = "",
    @JsonProperty("name") val name: String = "",
    @JsonProperty("avatar_url") val avatar_url: String = "",
): IzzyResource()