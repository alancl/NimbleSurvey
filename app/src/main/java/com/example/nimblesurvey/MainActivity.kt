package com.example.nimblesurvey

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme
import com.example.nimblesurvey.view.HomeScreen
import com.example.nimblesurvey.view.LoginScreen
import com.example.nimblesurvey.view.SplashScreen
import com.example.nimblesurvey.view.SurveyScreen
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.KoinContext

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            KoinContext {
                NimbleSurveyTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        AppNavigation()
                    }
                }
            }
        }
    }
}

sealed class Screen(
    val route: String,
) {
    data object Splash: Screen(route = "splash")
    data object Login: Screen(route = "login")
    data object Home: Screen(route = "home")
    data object Survey: Screen(route = "survey/{id}") {
        val args = listOf(navArgument("id") { type = NavType.StringType })
    }
    class SurveyId(id: String): Screen(route = "survey/$id")
}

@Composable
fun AppNavigation(start: Screen = Screen.Splash) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = start.route) {
        composable(Screen.Splash.route, exitTransition = { ExitTransition.None }) {
            SplashScreen(
                viewModel = koinViewModel(),
                onFinish = { loggedIn ->
                    val destination = if (loggedIn) Screen.Home else Screen.Login
                    navController.navigate(destination.route) {
                        popUpTo(Screen.Splash.route) {
                            inclusive = true
                        }
                    }
                },
            )
        }
        composable(Screen.Login.route, enterTransition = { EnterTransition.None }) {
            LoginScreen(
                viewModel = koinViewModel(),
                onLoginSuccess = {
                    navController.navigate(Screen.Home.route) {
                        popUpTo(Screen.Login.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(Screen.Home.route) {
            HomeScreen(
                viewModel = koinViewModel(),
                onSurveyClick = {
                    navController.navigate(Screen.SurveyId(it).route)
                },
                onLogoutClick = {
                    navController.navigate(Screen.Login.route) {
                        popUpTo(Screen.Home.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(Screen.Survey.route, Screen.Survey.args) {
            SurveyScreen(
                viewModel = koinViewModel(),
                surveyId = it.arguments?.getString("id")!!,
                onBackClick = {
                    navController.navigateUp()
                }
            )
        }
    }
}