package com.example.nimblesurvey

import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.model.Survey
import com.example.nimblesurvey.model.SurveyAnswer
import com.example.nimblesurvey.model.SurveyQuestion
import com.example.nimblesurvey.model.SurveySimple
import com.example.nimblesurvey.model.UserProfile
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.undabot.izzy.jackson.JacksonParser
import com.undabot.izzy.parser.Izzy
import com.undabot.izzy.parser.IzzyConfiguration
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response

fun <T, E: Throwable> Response<T>.asResult(converter: Converter<ResponseBody, E>): Result<T> =
    if (isSuccessful)
        body()?.let { Result.success(it) } ?: Result.failure(RuntimeException("null body"))
    else
        Result.failure(errorBody()?.let{ converter.convert(it) } ?: RuntimeException(message()))

fun <E: Throwable> Response<Void>.asUnitResult(converter: Converter<ResponseBody, E>): Result<Unit> =
    if (isSuccessful)
        Result.success(Unit)
    else
        Result.failure(errorBody()?.let{ converter.convert(it) } ?: RuntimeException(message()))

val moshi: Moshi = Moshi.Builder()
    .addLast(KotlinJsonAdapterFactory())
    .build()

val izzi = Izzy(JacksonParser(IzzyConfiguration(arrayOf(
    LoginToken::class.java,
    UserProfile::class.java,
    SurveySimple::class.java,
    Survey::class.java,
    SurveyQuestion::class.java,
    SurveyAnswer::class.java,
))))
