package com.example.nimblesurvey.repository

import android.content.Context
import com.example.nimblesurvey.ConverterFormat
import com.example.nimblesurvey.MultiConverterFactory
import com.example.nimblesurvey.R
import com.example.nimblesurvey.ResponseFormat
import com.example.nimblesurvey.asResult
import com.example.nimblesurvey.asUnitResult
import com.example.nimblesurvey.model.ApiErrors
import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.model.ResponseQuestion
import com.example.nimblesurvey.model.ResponseRequest
import com.example.nimblesurvey.model.Survey
import com.example.nimblesurvey.model.SurveyAnswer
import com.example.nimblesurvey.model.SurveyQuestion
import com.example.nimblesurvey.model.SurveySimple
import com.undabot.izzy.models.JsonDocument
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface SurveyApi {
    @GET("surveys")
    @ResponseFormat(ConverterFormat.JsonApi)
    suspend fun list(@Header("Authorization") auth: String): Response<JsonDocument<List<SurveySimple>>>

    @GET("surveys/{id}")
    @ResponseFormat(ConverterFormat.JsonApi)
    suspend fun detail(@Header("Authorization") auth: String, @Path("id") id: String): Response<JsonDocument<Survey>>

    @POST("responses")
    suspend fun response(@Header("Authorization") auth: String, @Body params: ResponseRequest): Response<Void>
}

interface SurveyRepository {
    suspend fun list(token: LoginToken): Result<JsonDocument<List<SurveySimple>>>

    suspend fun detail(token: LoginToken, surveyId: String): Result<JsonDocument<Survey>>

    suspend fun response(token: LoginToken, surveyId: String, questions: List<ResponseQuestion>): Result<Unit>
}

class SurveyRepositoryImpl(context: Context) : SurveyRepository {
    private val retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.api_url))
        .addConverterFactory(MultiConverterFactory())
        .build()
    private val api = retrofit.create<SurveyApi>()
    private val converter = retrofit.responseBodyConverter<ApiErrors>(ApiErrors::class.java, arrayOf())

    override suspend fun list(token: LoginToken) =
        api.list(auth = token.auth).asResult(converter)

    override suspend fun detail(token: LoginToken, surveyId: String) =
        api.detail(auth = token.auth, id = surveyId).asResult(converter)

    override suspend fun response(token: LoginToken, surveyId: String, questions: List<ResponseQuestion>) =
        api.response(
            auth = token.auth,
            params = ResponseRequest(
                survey_id = surveyId,
                questions = questions,
            ),
        ).asUnitResult(converter)
}

class FakeSurveyRepository: SurveyRepository {
    companion object {
        val simpleSurvey = SurveySimple(
            title = "Test Survey foo bar asdf",
            description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
        ).apply { id = "1" }

        val detailSurvey = Survey(
            title = simpleSurvey.title,
            description = simpleSurvey.description,
        ).apply {
            id = "1"
            questions = listOf(
                SurveyQuestion(
                    text = "How did you foo bar lorem ipsum?",
                    pick = "one",
                    display_type = "star",
                ).apply {
                    id = "1"
                    answers = List(5) { SurveyAnswer(text = "$it").apply { id = "$it" } }
                }
            )
        }
    }

    override suspend fun list(token: LoginToken): Result<JsonDocument<List<SurveySimple>>> {
        return Result.success(JsonDocument(data = listOf(simpleSurvey)))
    }

    override suspend fun detail(token: LoginToken, surveyId: String): Result<JsonDocument<Survey>> {
        return Result.success(JsonDocument(data = detailSurvey))
    }

    override suspend fun response(
        token: LoginToken,
        surveyId: String,
        questions: List<ResponseQuestion>
    ): Result<Unit> {
        return Result.failure(NotImplementedError("Not yet implemented"))
    }
}