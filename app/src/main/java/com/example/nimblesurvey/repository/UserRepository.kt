package com.example.nimblesurvey.repository

import android.content.Context
import com.example.nimblesurvey.ConverterFormat
import com.example.nimblesurvey.MultiConverterFactory
import com.example.nimblesurvey.R
import com.example.nimblesurvey.ResponseFormat
import com.example.nimblesurvey.asResult
import com.example.nimblesurvey.asUnitResult
import com.example.nimblesurvey.model.ApiErrors
import com.example.nimblesurvey.model.DataResponse
import com.example.nimblesurvey.model.ForgotRequest
import com.example.nimblesurvey.model.LoginRequest
import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.model.LogoutRequest
import com.example.nimblesurvey.model.RefreshRequest
import com.example.nimblesurvey.model.RegistrationRequest
import com.example.nimblesurvey.model.RegistrationUser
import com.example.nimblesurvey.model.UserEmail
import com.example.nimblesurvey.model.UserProfile
import com.undabot.izzy.models.JsonDocument
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface UserApi {
    @POST("oauth/token")
    @ResponseFormat(ConverterFormat.JsonApi)
    suspend fun login(@Body params: LoginRequest): Response<JsonDocument<LoginToken>>

    @POST("oauth/token")
    @ResponseFormat(ConverterFormat.JsonApi)
    suspend fun refreshToken(@Body params: RefreshRequest): Response<JsonDocument<LoginToken>>

    @POST("oauth/revoke")
    suspend fun logout(@Body params: LogoutRequest): Response<Void>

    @POST("passwords")
    suspend fun forgotPassword(@Body params: ForgotRequest): Response<DataResponse<Unit>>

    @POST("registrations")
    suspend fun register(@Body params: RegistrationRequest): Response<Void>

    @GET("me")
    @ResponseFormat(ConverterFormat.JsonApi)
    suspend fun profile(@Header("Authorization") auth: String): Response<JsonDocument<UserProfile>>
}

interface UserRepository {
    suspend fun login(email: String, password: String): Result<JsonDocument<LoginToken>>

    suspend fun refreshToken(token: String): Result<JsonDocument<LoginToken>>

    suspend fun logout(token: String): Result<Unit>

    suspend fun forgotPassword(email: String): Result<DataResponse<Unit>>

    suspend fun register(email: String, name: String, password: String, password2: String): Result<Unit>

    suspend fun profile(token: LoginToken): Result<JsonDocument<UserProfile>>
}

class UserRepositoryImpl(context: Context) : UserRepository {
    private val retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.api_url))
        .addConverterFactory(MultiConverterFactory())
        .build()
    private val api = retrofit.create<UserApi>()
    private val converter = retrofit.responseBodyConverter<ApiErrors>(ApiErrors::class.java, arrayOf())
    private val clientId: String = context.getString(R.string.client_id)
    private val clientSecret: String = context.getString(R.string.client_secret)

    override suspend fun login(email: String, password: String) =
        api.login(LoginRequest(
            grant_type = "password",
            email = email,
            password = password,
            client_id = clientId,
            client_secret = clientSecret,
        )).asResult(converter)

    override suspend fun refreshToken(token: String) =
        api.refreshToken(RefreshRequest(
            grant_type = "refresh_token",
            refresh_token = token,
            client_id = clientId,
            client_secret = clientSecret,
        )).asResult(converter)

    override suspend fun logout(token: String) =
        api.logout(LogoutRequest(
            token = token,
            client_id = clientId,
            client_secret = clientSecret,
        )).asUnitResult(converter)

    override suspend fun forgotPassword(email: String) =
        api.forgotPassword(ForgotRequest(
            user = UserEmail(email),
            client_id = clientId,
            client_secret = clientSecret,
        )).asResult(converter)

    override suspend fun register(email: String, name: String, password: String, password2: String) =
        api.register(RegistrationRequest(
            user = RegistrationUser(
                email = email,
                name = name,
                password = password,
                password_confirmation = password2,
            ),
            client_id = clientId,
            client_secret = clientSecret,
        )).asUnitResult(converter)

    override suspend fun profile(token: LoginToken) =
        api.profile(auth = token.auth).asResult(converter)
}

class FakeUserRepository: UserRepository {
    companion object {
        val userProfile = UserProfile(name = "Test User", email = "test@example.com")
    }

    override suspend fun login(email: String, password: String): Result<JsonDocument<LoginToken>> {
         return Result.success(JsonDocument(data = LoginToken()))
    }

    override suspend fun refreshToken(token: String): Result<JsonDocument<LoginToken>> {
        return Result.success(JsonDocument(data = LoginToken()))
    }

    override suspend fun logout(token: String): Result<Unit> {
        return Result.success(Unit)
    }

    override suspend fun forgotPassword(email: String): Result<DataResponse<Unit>> {
        return Result.failure(NotImplementedError("Not yet implemented"))
    }

    override suspend fun register(
        email: String,
        name: String,
        password: String,
        password2: String
    ): Result<Unit> {
        return Result.failure(NotImplementedError("Not yet implemented"))
    }

    override suspend fun profile(token: LoginToken): Result<JsonDocument<UserProfile>> {
        return Result.success(JsonDocument(data = userProfile))
    }
}
