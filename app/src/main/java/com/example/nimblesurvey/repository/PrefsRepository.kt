package com.example.nimblesurvey.repository

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.moshi
import com.squareup.moshi.adapter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map

interface PrefsRepository {
    val loginToken: Flow<LoginToken?>

    suspend fun setLoginToken(token: LoginToken?)
}

class PrefsRepositoryImpl(private val context: Context) : PrefsRepository {
    companion object {
        private val Context.dataStore by preferencesDataStore(name = "prefs")
        private val loginKey = stringPreferencesKey("login")

        @OptIn(ExperimentalStdlibApi::class)
        private val tokenAdapter = moshi.adapter<LoginToken>()
    }

    override val loginToken = context.dataStore.data
        .map { prefs -> prefs[loginKey]?.let(tokenAdapter::fromJson) }

    override suspend fun setLoginToken(token: LoginToken?) {
        context.dataStore.edit { prefs ->
            prefs[loginKey] = tokenAdapter.toJson(token)
        }
    }
}

class FakePrefsRepository: PrefsRepository {
    private val tokenFlow = MutableStateFlow<LoginToken?>(LoginToken())

    override val loginToken: Flow<LoginToken?> = tokenFlow

    override suspend fun setLoginToken(token: LoginToken?) {
        tokenFlow.value = token
    }
}