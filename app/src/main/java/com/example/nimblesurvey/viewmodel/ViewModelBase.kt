package com.example.nimblesurvey.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.repository.PrefsRepository
import com.example.nimblesurvey.repository.UserRepository
import kotlinx.coroutines.flow.first

open class ViewModelBase(
    protected val user: UserRepository,
    protected val prefs: PrefsRepository,
): ViewModel() {
    protected var loginToken: LoginToken? = null

    suspend fun getRefreshedToken(): LoginToken? {
        if (loginToken == null)
            loginToken = prefs.loginToken.first()
        return loginToken?.let { token ->
            if (token.expired)
                user.refreshToken(token.refresh_token)
                    .map { it.data!! }
                    .onSuccess {
                        prefs.setLoginToken(it)
                        loginToken = it
                        Log.d("getRefreshedToken", "login token refreshed: $it")
                    }
                    .getOrNull() //FIXME: failure should trigger a logout
            else
                token
        }
    }
}