package com.example.nimblesurvey.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.example.nimblesurvey.model.ResponseAnswer
import com.example.nimblesurvey.model.ResponseQuestion
import com.example.nimblesurvey.model.Survey
import com.example.nimblesurvey.model.SurveyQuestion
import com.example.nimblesurvey.repository.PrefsRepository
import com.example.nimblesurvey.repository.SurveyRepository
import com.example.nimblesurvey.repository.UserRepository

class SurveyViewModel(
    user: UserRepository,
    prefs: PrefsRepository,
    private val surveyRepo: SurveyRepository,
): ViewModelBase(user, prefs) {
    var survey: Survey? by mutableStateOf(null)
        private set
    var questionIndex: Int? by mutableStateOf(null)
    val currentQuestion: SurveyQuestion?
        get() = questionIndex?.let { survey?.questions?.get(it) }

    val singleStorage = mutableStateMapOf<String, String>()                 // question.id -> answer.id
    val multiStorage = mutableStateMapOf<Pair<String, String>, Boolean>()   // (question.id, answer.id) -> selected
    val textStorage = mutableStateMapOf<Pair<String, String>, String>()     // (question.id, answer.id) -> text

    suspend fun loadSurvey(surveyId: String) {
        survey = getRefreshedToken()
            ?.let { surveyRepo.detail(it, surveyId) }
            ?.getOrNull()?.data
    }

    suspend fun submitSurveyResponses(): Result<Unit> =
        getRefreshedToken()
            ?.let { token ->
                survey?.let { surv ->
                    surveyRepo.response(
                        token = token,
                        surveyId = surv.id!!,
                        questions = surv.questions.map { question ->
                            ResponseQuestion(
                                id = question.id!!,
                                answers = singleStorage.toMap().mapNotNull {
                                    if (it.key == question.id) ResponseAnswer(id = it.value) else null
                                } + multiStorage.toMap().mapNotNull {
                                    if (it.key.first == question.id) ResponseAnswer(id = it.key.second) else null
                                } + textStorage.toMap().mapNotNull {
                                    if (it.key.first == question.id) ResponseAnswer(id = it.key.second, answer = it.value) else null
                                }
                            )
                        }.filter { it.answers.isNotEmpty() }
                    )
                }
            }
            ?: Result.failure(RuntimeException("not logged in"))

    fun clearSurveyResponses() {
        singleStorage.clear()
        multiStorage.clear()
        textStorage.clear()
    }
}