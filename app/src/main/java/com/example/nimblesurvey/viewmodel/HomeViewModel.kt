package com.example.nimblesurvey.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.example.nimblesurvey.model.SurveySimple
import com.example.nimblesurvey.model.UserProfile
import com.example.nimblesurvey.repository.PrefsRepository
import com.example.nimblesurvey.repository.SurveyRepository
import com.example.nimblesurvey.repository.UserRepository

class HomeViewModel(
    user: UserRepository,
    prefs: PrefsRepository,
    private val survey: SurveyRepository,
): ViewModelBase(user, prefs) {
    var userProfile: UserProfile? by mutableStateOf(null)
        private set
    var surveys by mutableStateOf(listOf<SurveySimple>())
        private set

    suspend fun logout(): Result<Unit> =
        getRefreshedToken()
            ?.let { user.logout(it.access_token) }
            ?.onSuccess { prefs.setLoginToken(null) }
            ?: Result.success(Unit)

    suspend fun loadUserProfile() {
        userProfile = getRefreshedToken()
            ?.let { user.profile(it).getOrNull()?.data }
    }

    suspend fun loadSurveys() {
        surveys = getRefreshedToken()
            ?.let { survey.list(it).getOrNull()?.data }
            ?: listOf()
    }
}