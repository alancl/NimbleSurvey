package com.example.nimblesurvey.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.repository.PrefsRepository
import com.example.nimblesurvey.repository.UserRepository

class LoginViewModel(
    private val user: UserRepository,
    private val prefs: PrefsRepository,
): ViewModel() {
    var email by mutableStateOf("")
    var passwd by mutableStateOf("")

    suspend fun login(): Result<LoginToken> =
        user.login(email, passwd)
            .map { it.data!! }
            .onSuccess { prefs.setLoginToken(it) }
}
