package com.example.nimblesurvey.viewmodel

import androidx.lifecycle.ViewModel
import com.example.nimblesurvey.model.LoginToken
import com.example.nimblesurvey.repository.PrefsRepository
import com.example.nimblesurvey.repository.UserRepository
import kotlinx.coroutines.flow.first

class SplashViewModel(
    private val user: UserRepository,
    private val prefs: PrefsRepository,
): ViewModel() {
    suspend fun getLoginToken(): LoginToken? =
        prefs.loginToken.first()

    suspend fun refreshToken(token: LoginToken): Result<LoginToken> =
        user.refreshToken(token.refresh_token)
            .map { it.data!! }
            .onSuccess { prefs.setLoginToken(it) }
}