package com.example.nimblesurvey

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nimblesurvey.repository.FakePrefsRepository
import com.example.nimblesurvey.repository.FakeSurveyRepository
import com.example.nimblesurvey.repository.FakeUserRepository
import com.example.nimblesurvey.ui.theme.NimbleSurveyTheme
import com.example.nimblesurvey.view.HomeScreen
import com.example.nimblesurvey.viewmodel.HomeViewModel
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ComposeInstrumentedTest {
    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun homeScreenTest() {
        var surveyId: String? = null
        var logoutClicked = false

        composeTestRule.activity.setContent {
            NimbleSurveyTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background,
                ) {
                    HomeScreen(
                        viewModel = HomeViewModel(
                            user = FakeUserRepository(),
                            prefs = FakePrefsRepository(),
                            survey = FakeSurveyRepository(),
                        ),
                        onSurveyClick = { surveyId = it },
                        onLogoutClick = { logoutClicked = true },
                    )
                }
            }
        }

        val survey = FakeSurveyRepository.simpleSurvey
        composeTestRule.onNodeWithTag("survey_title")
            .assertTextEquals(survey.title)
        composeTestRule.onNodeWithTag("survey_desc")
            .assertTextEquals(survey.description)
        composeTestRule.onNodeWithTag("floating_button")
            .performClick()
        assert(surveyId != null)
        assert(surveyId == survey.id)
        composeTestRule.onNodeWithTag("user_icon")
            .performClick()
        composeTestRule.onNodeWithTag("user_name")
            .assertTextEquals(FakeUserRepository.userProfile.name)
        composeTestRule.onNodeWithTag("logout")
            .performClick()
        assert(logoutClicked)
    }
}
